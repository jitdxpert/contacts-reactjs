import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import './assets/css/app.min.css';

import Navbar from './components/common/navbar';
import Home from './components/pages/home';
import Contacts from './components/pages/contacts/contacts';
import Products from './components/pages/products';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">

          <Navbar />

          <Route exact path="/" component={Home}></Route>
          <Route exact path="/contacts" component={Contacts}></Route>
          <Route exact path="/products" component={Products}></Route>

        </div>
      </Router>
    );
  }
}

export default App;
