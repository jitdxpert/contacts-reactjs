import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: false
    };

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {
    let collapseClasses = 'collapse navbar-collapse ' + (this.state.isToggleOn ? 'show' : '');
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container">
          <Link to="/" className="navbar-brand">MEAN APP</Link>
          <button className="navbar-toggler collapsed" type="button" onClick={this.handleClick}>
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className={collapseClasses}>
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to="/contacts" className="nav-link">Contacts</Link>
              </li>
              <li className="nav-item">
                <Link to="/products" className="nav-link">Products</Link>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" placeholder="Search" type="text" />
              <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
