import React, { Component } from 'react';

class Contact extends Component {
  render() {
    return (
      <div className="card bg-light mb-3">
        <div className="card-header">Add Contact</div>
        <div className="card-body">
          <form method="post">
            <div className="form-group">
              <label htmlFor="first_name">First Name</label>
              <input type="text" name="first_name" className="form-control" required />
            </div>
            <div className="form-group">
              <label htmlFor="last_name">Last Name</label>
              <input type="text" name="last_name" className="form-control" required />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Phone</label>
              <input type="text" name="phone" className="form-control" required />
            </div>
            <div className="row form-group pt-3 border-top">
              <div className="col">
                <button type="submit" className="btn btn-primary btn-block">SAVE</button>
              </div>
              <div className="col">
                <button type="reset" className="btn btn-secondary btn-block">CANCEL</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default Contact;
