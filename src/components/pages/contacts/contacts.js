import React, { Component } from 'react';
import axios from 'axios';

import Contact from './contact';

class Contacts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contacts: []
    };
  }

  componentDidMount() {
    this.getContacts();
  }

  getContacts() {
    axios.get('http://localhost:4100/api/contacts', {
      responseType: 'json'
    }).then(res => {
      this.setState({
        contacts: res.data
      });
    });
  }

  deleteContact(id){
    axios.delete('http://localhost:4100/api/contact/'+id).then(res => {
      this.getContacts();
      console.log('Contact deleted successfully.');
    });
  }

  render() {
    const { contacts } = this.state;

    let contactItems = '';
    if(contacts.length){
      contactItems = contacts.map(contact =>
        <tr key={contact._id}>
          <td>{contact.first_name}</td>
          <td>{contact.last_name}</td>
          <td>{contact.phone}</td>
          <td>
            <button type="button" className="btn btn-sm btn-danger" onClick={this.deleteContact.bind(this, contact._id)}>Delete</button>
          </td>
        </tr>
      );
    }else{
      contactItems = (
        <tr>
          <td colSpan="4" className="text-center">No contacts available</td>
        </tr>
      );
    }

    return (
      <div className="container">
        <div className="mt-5"></div>

        <Contact />

        <div className="card bg-light mb-3">
          <div className="card-header">Contacts</div>
          <div className="card-body">
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Phone</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {contactItems}
              </tbody>
            </table>
          </div>
        </div>
      </div>

    );
  }
}

export default Contacts;
